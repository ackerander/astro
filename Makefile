OBJ = main.o render.o game.o
DEST = game
INCS = -lSDL2 -lSDL2_gfx -lm
CFLAGS = -std=c99 -Wall -Wextra -pedantic -O3 -march=native -mtune=native
CC = cc

all: ${OBJ}
	${CC} ${INCS} -o ${DEST} ${OBJ}

main.o: main.c game.h
	${CC} ${CFLAGS} -c -o $@ $<

render.o: render.c game.h
	${CC} ${CFLAGS} -c -o $@ $<

game.o: game.c game.h
	${CC} ${CFLAGS} -c -o $@ $<

clean:
	rm ${OBJ}
