#ifndef GAME_H
#define GAME_H

#include <SDL2/SDL.h>
#define BDY(I) game.bodies[(I)]

enum state { QUIT = 0, RUNNING = 1 };

typedef struct {
	double r;
	double m;
	double px;
	double py;
	double vx;
	double vy;
} body_t;

typedef struct {
	body_t *bodies;
	size_t nBodies;
	double g;
	char status;
} game_t;

void newGame();
int physicsLoop();
void threadPhysics();
void quitGame();

char init(double zoom);
int ioLoop(void *);
void threadIO();
void quit(char code);

#endif
